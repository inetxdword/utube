# -*- coding: utf-8 -*-
# InetxDword

from __future__ import unicode_literals
import youtube_dl
import os
import sys
import progressbar
from utubedb import *
import lxml
from lxml import etree
import urllib
import codecs
from colorama import init, Fore, Back, Style

class UtubeLogger(object):
	'''...'''
	def debug(self, msg):
		pass

	def warning(self, msg):
		pass

	def error(self, msg):
		print(msg)		

def progressBar(progress, progressAsciiFill = '*', progressAsciiLBorder = '|', progressAsciiRBorder = '|'):
	#WHILE FILE IS DOWNLOADING...
	if(progress['status'] == 'downloading'):
		#CALCULATE MB OF THE MP3 FILE
		utubeBytes = ((progress['downloaded_bytes']) * 100) / int(progress['total_bytes'])
		progressAsciiFill = progressAsciiFill * ((utubeBytes*20)/100) + (" " * (20 - ((utubeBytes*20)/100)))

		#PRINT PROGRESS
		sys.stdout.write('\r')
		output = "[+] " + progressAsciiLBorder + progressAsciiFill + progressAsciiRBorder + " " + str(utubeBytes) + "% of " + str(round(int(progress['total_bytes'])/1048576.0, 1)) + "MiB "
		sys.stdout.write(output)
		sys.stdout.flush()

	#FILE DOWNLOADED.
	if(progress['status'] == 'finished'):
		print("\n[+] Converting...")

class Utube(object):
	"""UTUBE DOWNLOADER WITH COMMAND LINE FOR WINDOWS.

	Use commands for download mp3 songs. Just type youtube's url.
	For example:
	Command: add https://www.youtube.com/watch?v=RRHc_tDfvgs
	Command: go
	Start download...

	Attributes:
		None
	"""
	def __init__(self):
		"""Inits db connection to sqlite3 for save or load a preview session."""
		self.db = DbLinks()
		self.urls = []		
		self.db.initSession(self.urls)
		self.count = 0
		self.command = '|' + Back.GREEN + ' ' + Back.WHITE + Fore.BLACK + Style.BRIGHT + '*' + Back.RED + ' ' + Style.RESET_ALL + '| Command: '  
		self.USER_PATH = os.environ['USERPROFILE'] + "\Music\\"
		self.none_ascii_subdir = " └─ "	
		self.config()	

	def config(self):
		"""Youtube-dl params."""
		self.config = {
			'format' 				: 'bestaudio/best',
			'extractaudio' 			: True,
			'audioformat' 			: 'mp3',
			'outtmpl'				: self.USER_PATH + '\\' + u'%(title)s.%(ext)s', #-%(id)s
			'noplaylist' 			: True,
			'nocheckcertificate'	: True,
			'no_warnings'			: True,
			'logger'				: UtubeLogger(),
			'progress_hooks'		: [progressBar],
			'postprocessors' 		: [{
				'key' 				: 'FFmpegExtractAudio',
				'preferredcodec' 	: 'mp3',
				'preferredquality' 	: '192',
			}],
		}

	def printSubdir(self):
		"""Print directories inside PATH output file where mp3 files are downloaded."""
		count = 0

		#PRINT SUBDIRS
		for path in [x[0] for x in os.walk(self.USER_PATH)]:
			if(count > 0):#Subdirectories
				print(self.none_ascii_subdir + path)
			else:#Main Directory
				print("-+ " + path)
			count = count + 1

	def processCommand(self):
		"""Read & check available command and execute a task."""

		#READ COMMAND	
		cmd = raw_input(self.command).split()
		cmd[0] = cmd[0].upper()

		#CHECK COMMAND
		#-----------------------------------------------------------------------------
		#ADD URL TO URLS BUFFER WILL BE DOWNLOADED
		if(cmd[0] == 'ADD'):
			self.urls.append(cmd[1])
			self.db.addUrl(cmd[1])
		#-----------------------------------------------------------------------------
		#PRINT TITLE YOUTUBE VIDEOS			
		elif(cmd[0] == 'SHOW'):
			for url in self.urls:
				self.downloadInfo(url)
		#-----------------------------------------------------------------------------
		#PRINT MAIN PATH				
		elif(cmd[0] == 'PATH'):
			print("-+ " + self.USER_PATH)
		#-----------------------------------------------------------------------------		
		#SET NEW MAIN PATH	
		elif(cmd[0] == 'SET' and cmd[1].upper() == 'PATH'):
			self.printSubdir()

			#USER WRITES NEW PATH
			self.USER_PATH = raw_input('PATH: ') + "\\"
			
			print("-+ PATH CHANGED")
			print("-+ " + self.USER_PATH)
			
			#CONFIG NEW PATH
			self.config()
		#-----------------------------------------------------------------------------
		#PRINT SUBDIRECTORIES OF MAIN PATH
		elif(cmd[0] == 'SPATH'):
			self.printSubdir()
		#-----------------------------------------------------------------------------	
		#PRINT HELP		
		elif(cmd[0] == 'SOS'):
			for command in ['ADD youtube_url', 'SHOW', 'PATH', 'SPATH', 'SOS','SESSIONS', 'DELETE SESSION session_index', 'EXIT/QUIT']:
				print("+- " + command)	
		#-----------------------------------------------------------------------------
		#FINISH PROGRAM. CURRENT SESSION IS STORED IN DB
		elif(cmd[0] == 'EXIT' or cmd[0] == 'QUIT'):
			self.db.Close()
			sys.exit()
		#-----------------------------------------------------------------------------
		#PRINT DB SESSIONS SAVED
		elif(cmd[0] == 'SESSIONS'):
			self.db.printSessions()
		#-----------------------------------------------------------------------------
		#PRINT CURRENT DB SESSION
		elif(cmd[0] == 'SESSION'):
			print("[+] " + self.db.current_session)
		#-----------------------------------------------------------------------------
		#DROP SESSION INDEX			
		elif(cmd[0] == 'DELETE' and cmd[1].upper() == 'SESSION'):
			self.db.deleteSession(cmd[2])
			self.urls = []
			self.db.initSession(self.urls)
		#-----------------------------------------------------------------------------
		#PRINT EASTERN EGG
		elif(cmd[0] == 'INETXDWORD'):
			self.easternEgg()
		#-----------------------------------------------------------------------------			
		return cmd[0]

	def main(self):
		'''Run program'''

		#PRINT BANNER
		print ''
		print '[D]Tube AntiCopyright.'
		print '[E]nter urls.'
		print '[E]nter \'GO\' for start download. \'SOS\' for help.'
		print '[D]ownload now motherfucker.'		
		print ''

		#READ FIRST COMMAND
		varCommand = self.processCommand()
		#READ COMMAND UNTIL FIND 'GO'.
		while varCommand != 'GO':
			varCommand = self.processCommand()
		
		#START DOWNLOAD
		if len(self.urls) > 0:
			for url in self.urls:
				print('\n+----------------------------------------------------------+')
				print "[+] Downloading: " + url
				self.downloadInfo(url)
				with youtube_dl.YoutubeDL(self.config) as buffer:
					buffer.download([url])
		print('+----------------------------------------------------------+')

	def downloadInfo(self, url):
		'''Get youtube video title and print it.'''

		#YOUTUBE-DL PARAMS
		configTitle = {
			'format' 				: 'bestaudio/best',
			'extractaudio' 			: False,
			'audioformat' 			: 'mp3',
			'outtmpl'				:u'%(title)s.%(ext)s', #-%(id)s
			'noplaylist' 			: True,
			'nocheckcertificate'	: True,
			'no_warnings'			: True,
			'logger'				: UtubeLogger(),
			'progress_hooks'		: [progressBar]		
		}	

		#DOWNLOAD JUST TITLE
		with youtube_dl.YoutubeDL(configTitle) as ydl:
			info_dict = ydl.extract_info(url, download=False)
			video_title = info_dict.get('title', None)
			print('[+] Título: ' + video_title)

	def easternEgg(self):
		print '''

.__               __             .___                       .___
|__| ____   _____/  |____  ___ __| _/_  _  _____________  __| _/
|  |/    \_/ __ \   __\  \/  // __ |\ \/ \/ /  _ \_  __ \/ __ | 
|  |   |  \  ___/|  |  >    </ /_/ | \     (  <_> )  | \/ /_/ | 
|__|___|  /\___  >__| /__/\_ \____ |  \/\_/ \____/|__|  \____ | 
        \/     \/           \/    \/                         \/ 		
.     .       .  .   . .   .   . .    +  .
  .     .  :     .    .. :. .___---------___.
       .  .   .    .  :.:. _".^ .^ ^.  '.. :"-_. .
    .  :       .  .  .: ./:            . .^  :.:\.
        .   . :: +. :.:/: .   .    .        . . .:\.
 .  :    .     . _ :::/:               .  ^ .  . .:\.
  .. . .   . - : :.:./.                        .  .:\.
  .      .     . :..|:                    .  .  ^. .:|
    .       . : : ..||        .                . . !:|
  .     . . . ::. ::\(                           . :)/
 .   .     : . : .:.|. ######              .#######::|
  :.. .  :-  : .:  ::|.#######           ..########:|
 .  .  .  ..  .  .. :\ ########          :######## :/
  .        .+ :: : -.:\ ########       . ########.:/
    .  .+   . . . . :.:\. #######       #######..:/
      :: . . . . ::.:..:.\           .   .   ..:/
   .   .   .  .. :  -::::.\.       | |     . .:/
      .  :  .  .  .-:.":.::.\             ..:/
 .      -.   . . . .: .:::.:.\.           .:/
.   .   .  :      : ....::_:..:\   ___.  :/
   .   .  .   .:. .. .  .: :.:.:\       :/
     +   .   .   : . ::. :.:. .:.|\  .:/|
     .         +   .  .  ...:: ..|  --.:|
.      . . .   .  .  . ... :..:.."(  ..)"
 .   .       .      :  .   .: ::/  .  .::\
		'''

if __name__ == '__main__':
	init()
	utube = Utube()
	utube.main()