# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import sqlite3
import uuid
import os

class DbLinks(object):
	def __init__(self):
		self.pwd = os.path.dirname(os.path.abspath(__file__))
		self.db = sqlite3.connect(self.pwd + '\\udb.db')
		self.cursor = self.db.cursor()
		self.none_ascii_subdir = " └─ "
		self.current_session = None

	def initSession(self, urls):
		self.cursor.execute('SELECT SEON_SESSIONID FROM SE_SESSION WHERE SEON_STATUS = 1')
		if(len(self.cursor.fetchall()) > 0):
			print("[+] Active sessions were found")
			self.printSessions()
			self.setSession(urls)
		else:
			self.current_session = uuid.uuid4().hex
			self.cursor.execute('INSERT INTO SE_SESSION(SEON_SESSIONID, SEON_STATUS) VALUES(?,?)', (self.current_session, 1))
			self.db.commit()

			print("[+] Session {0} registred.".format(self.current_session))

	def setSession(self, urls):
		rows = []

		while len(rows) == 0:
			index = raw_input('Choose session index (0 for new session): ')

			if(index == '0'):
				self.current_session = uuid.uuid4().hex
				self.cursor.execute('INSERT INTO SE_SESSION(SEON_SESSIONID, SEON_STATUS) VALUES(?,?)', (self.current_session, 1))
				self.db.commit()
				print("[+] Session {0} registred.".format(self.current_session))
				break

			self.cursor.execute('SELECT SEON_SESSIONID FROM SE_SESSION WHERE SEON_STATUS = 1 AND SEON_INDEX = ?', (index,))
			rows = self.cursor.fetchall()

			if(len(rows) == 0):
				print("[-] Session index not found, try again.")
			else:
				self.current_session = rows[0][0]

				self.cursor.execute('SELECT LIUR_LINK FROM LI_URLS WHERE LIUR_SEON_SESSIONID = ?', (self.current_session,))
				links = self.cursor.fetchall()

				for link in links:
					urls.append(link[0])

	def printSessions(self):
		self.cursor.execute('SELECT SEON_SESSIONID, SEON_INDEX FROM SE_SESSION WHERE SEON_STATUS = 1')
		rows = self.cursor.fetchall()

		print("[+] Active sessions")
		for row in rows:
			session = row[0]
			sessionIndex = str(row[1])
			print(self.none_ascii_subdir + '[+] Index: ' + sessionIndex + '\t' + session)

			self.cursor.execute('SELECT LIUR_LINK FROM LI_URLS WHERE LIUR_SEON_SESSIONID = ?', (session,))
			links = self.cursor.fetchall()

			for link in links:
				utubeLink = link[0]
				print('    ' + self.none_ascii_subdir + ' ' + utubeLink)

	def addUrl(self, url):
		self.cursor = self.db.cursor()
		self.cursor.execute('INSERT INTO LI_URLS(LIUR_LINK, LIUR_SEON_SESSIONID) VALUES(?,?)', (url, self.current_session))
		self.db.commit()

	def Close(self):
		self.db.close()

	def deleteSession(self, sessionIndex):
		self.cursor = self.db.cursor()
		self.cursor.execute('SELECT SEON_SESSIONID FROM SE_SESSION WHERE SEON_INDEX = ?', (sessionIndex,))
		session = self.cursor.fetchall()
		if(len(session) == 0):
			print("[-] Session index not found, try again.")
		else:
			session = session[0][0]
			self.cursor.execute('DELETE FROM LI_URLS WHERE LIUR_SEON_SESSIONID = ?', (session,))
			self.cursor.execute('DELETE FROM SE_SESSION WHERE SEON_SESSIONID = ?', (session,))			
			self.db.commit()
			print("[+] Session " + session + " droped")